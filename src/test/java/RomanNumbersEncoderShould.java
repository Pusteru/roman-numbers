import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RomanNumbersEncoderShould {

    private Conversion romanNumbersEncoder = new Conversion();

    @Test
    public void shouldCovertToRoman() {
        assertEquals("solution(1) should equal to I", "I", this.romanNumbersEncoder.solution(1));
        assertEquals("solution(4) should equal to IV", "IV", this.romanNumbersEncoder.solution(4));
        assertEquals("solution(6) should equal to VI", "VI", this.romanNumbersEncoder.solution(6));
        assertEquals("solution(14) should equal to XIV", "XIV", this.romanNumbersEncoder.solution(14));
    }

    @Test(expected = NumberFormatException.class)
    public void throw_exception_overflow_number() {
        this.romanNumbersEncoder.solution(3999);
    }

    @Test(expected = NumberFormatException.class)
    public void throw_exception_underflow_number() {
        this.romanNumbersEncoder.solution(0);
    }


}


