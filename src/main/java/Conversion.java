class Conversion {

    private static String[] romanNumbers = {"I", "V", "X", "L", "C", "D", "M"};

    String solution(int integer) {
        StringBuilder romanNumber = new StringBuilder();
        int div = 1000;
        if (integer > 3999 || integer < 1) throw new NumberFormatException();
        int i = 6;
        while (div > 0) {
            int number = integer / div;
            if (number == 0) romanNumber.append("");
            else if (number < 4) romanNumber.append(numberTimes(romanNumbers[i], number));
            else if (number == 4) romanNumber.append(romanNumbers[i]).append(romanNumbers[i + 1]);
            else if (number < 9) romanNumber.append(romanNumbers[i + 1]).append(numberTimes(romanNumbers[i], number));
            else romanNumber.append(romanNumbers[i]).append(romanNumbers[i + 2]);
            integer -= number * div;
            div = div / 10;
            i -= 2;
        }

        return romanNumber.toString();
    }

    private String numberTimes(String romanNumber, int number) {
        StringBuilder numberTimes = new StringBuilder();
        for (int i = 0; i < number % 5; i++) {
            numberTimes.append(romanNumber);
        }
        return numberTimes.toString();
    }

}
