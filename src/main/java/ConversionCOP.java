import java.util.Map;
import java.util.TreeMap;

class ConversionCOP {


    private static Map<Integer, String> romanNumberByIntegers = new TreeMap<Integer, String>();

    ConversionCOP() {
        romanNumberByIntegers.put(3000, "M");
        romanNumberByIntegers.put(2000, "M");
        romanNumberByIntegers.put(1000, "M");
        romanNumberByIntegers.put(900, "DM");
        romanNumberByIntegers.put(800, "DCCC");
        romanNumberByIntegers.put(700, "DCC");
        romanNumberByIntegers.put(600, "DC");
        romanNumberByIntegers.put(500, "D");
        romanNumberByIntegers.put(400, "DC");
        romanNumberByIntegers.put(300, "CCC");
        romanNumberByIntegers.put(200, "CC");
        romanNumberByIntegers.put(100, "C");
        romanNumberByIntegers.put(90, "XC");
        romanNumberByIntegers.put(80, "LXXX");
        romanNumberByIntegers.put(70, "LXX");
        romanNumberByIntegers.put(60, "LX");
        romanNumberByIntegers.put(50, "L");
        romanNumberByIntegers.put(40, "XL");
        romanNumberByIntegers.put(30, "XXX");
        romanNumberByIntegers.put(20, "XX");
        romanNumberByIntegers.put(10, "X");
        romanNumberByIntegers.put(9, "IX");
        romanNumberByIntegers.put(8, "VIII");
        romanNumberByIntegers.put(7, "VII");
        romanNumberByIntegers.put(6, "VI");
        romanNumberByIntegers.put(5, "V");
        romanNumberByIntegers.put(4, "IV");
        romanNumberByIntegers.put(3, "III");
        romanNumberByIntegers.put(2, "II");
        romanNumberByIntegers.put(1, "I");
    }

    String solution(int integer) {
        StringBuilder romanNumber = new StringBuilder();
        int div = 1000;
        if (integer > 3999 || integer < 1) throw new NumberFormatException();
        for (int i = 0; i < 4; i++) {
            int number = integer / div * div;
            romanNumber.append(getNumeral(romanNumberByIntegers.get(number)));
            integer -= number;
            div = div / 10;
        }

        return romanNumber.toString();
    }

    private String getNumeral(String unity) {
        if (unity == null) {
            unity = "";
        }
        return unity;
    }


}
